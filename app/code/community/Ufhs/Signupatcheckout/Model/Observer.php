<?php
/*
* Ufhs_Signupatcheckout
* @category   Ufhs
* @package    Ufhs_Signupatcheckout
* @copyright  Copyright (c) 2017 Ufhs
* @license    https://bitbucket.org/underfloorheating/sign_up_at_checkout/blob/master/LICENSE.md
* @version    1.0.1
*/
class Ufhs_Signupatcheckout_Model_Observer
{
	/**
	* Subscribe customer email to newsletter if checkbox checked
	*
	* @param $observer
	* @return $this
	*/
	public function subscribeCustomerToNewsletter($observer)
	{
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		$customerEmail = $quote->getCustomerEmail();
		if (!$customerEmail) {
			$customerEmail = $quote->getBillingAddress()->getEmail();
		}

		$subscriberModel = Mage::getModel('newsletter/subscriber');
		$subscriber = $subscriberModel->loadByEmail($customerEmail);

		if (Mage::app()->getRequest()->getParam('is_subscribed')) {
			if (!$subscriber->getId()) {
				$subscriberModel->subscribe($customerEmail);
			} else {
				$subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
				$subscriber->save();
			}
		}

		return $this;
	}
}