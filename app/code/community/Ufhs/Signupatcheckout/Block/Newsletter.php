<?php
/*
 * Ufhs_Signupatcheckout
 * @category   Ufhs
 * @package    Ufhs_Signupatcheckout
 * @copyright  Copyright (c) 2017 Ufhs
 * @license    https://bitbucket.org/underfloorheating/sign_up_at_checkout/blob/master/LICENSE.md
 * @version    1.0.1
 */
class Ufhs_Signupatcheckout_Block_Newsletter extends Mage_Core_Block_Template
{
    /**
     * Check if module has been enabled in the admin
     *
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('newsletter/checkout/enable');
    }

    /**
     * Check if the newsletter checkbox should be checked by default
     *
     * @return bool
     */
    public function isCheckboxCheckedByDefault()
    {
        return Mage::getStoreConfigFlag('newsletter/checkout/checked');
    }

    /**
     * Get the newsletter checkbox label text
     *
     * @return string
     */
    public function getCheckboxLabelText()
    {
        return $this->escapeHtml(Mage::getStoreConfig('newsletter/checkout/text'));
    }
}