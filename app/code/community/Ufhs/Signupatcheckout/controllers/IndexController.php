<?php
/**
 * Onepage controller for checkout
 *
 * @category    Ufhs
 * @package     Ufhs_Signupatcheckout
 */
class Ufhs_Signupatcheckout_IndexController extends Mage_Core_Controller_Front_Action
{
	const STATUS_SUBSCRIBE = 1;
	const STATUS_UNSUBSCRIBE = 0;

    /**
     * Save checkout sign up subscription
     */
    public function saveSubscriptionAction()
    {
        if ($this->getRequest()->isPost()) {

            $choice = (int) $this->getRequest()->getPost('choice', self::STATUS_UNSUBSCRIBE);

            if($choice !== self::STATUS_SUBSCRIBE && $choice !== self::STATUS_UNSUBSCRIBE) {
            	echo 0;
            	return false;
            }

            $quote = Mage::getSingleton('checkout/session')->getQuote();
			$customerEmail = $quote->getCustomerEmail();
			if (!$customerEmail) {
				$customerEmail = $quote->getBillingAddress()->getEmail();
			}

			$subscriberModel = Mage::getModel('newsletter/subscriber');
			$subscriber = $subscriberModel->loadByEmail($customerEmail);

			if (!$subscriber->getId()) {
				// Subscriber entry doesn't exist yet, so create one
				if($choice == self::STATUS_SUBSCRIBE) {
					$subscriberModel->subscribe($customerEmail);
				}
			} else {

				$subscriberStatus = $choice == self::STATUS_SUBSCRIBE
									? Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED
									: Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED;

				$subscriber->setStatus($subscriberStatus);
				$subscriber->save();
			}
        }
    }
}
